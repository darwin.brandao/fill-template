#!/bin/bash

# Usage example: ./fill-template.sh -f path/to/template-t {} title="This is the title" content="This is the content" key1="value1" key2="value2" ...

# Inputs:
# $1 = Template path

# Input check
[ $# -eq 0 ] && echo "./fill-template.sh -f path/to/template -t {} title="This is the title" content="This is the content" key1="value1" key2="value2" ..." && exit 1

# Variables
tag="{}"

# https://stackoverflow.com/a/63653944
while [ $# -gt 0 ]; do
  case "$1" in
    -o | --output)
      output="${2}"
      ;;
    -f | --file)
      file="${2}"
      image="$( cat "${2}" )"
      ;;
    -t | --template-tag)
      tag="${2:0:2}"
      ;;
    *=*)
      key_values+="$1\n"
      ;;
    *)
  esac
  shift
done

key_values="$( echo -e "$key_values" )"

# Replace attributes
while IFS= read -r line; do
  # Replace tags
	image="${image//${tag:0:1}${line%=*}${tag:1:2}/${line#*=}}"

  # Replace tags hexadecimal
  image="${image//$( printf "%%%x" "'${tag:0:1}" | tr [a-z] [A-Z] )${line%=*}$( printf "%%%x" "'${tag:1:2}" | tr [a-z] [A-Z] )/${line#*=}}"
done <<< "$key_values"

echo "$image"
